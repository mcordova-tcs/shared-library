def publishAKS(Map param = [:]) {
    def azCredentials = param.get("azCredentials")
    def resourceGroup = param.get("resourceGroup")
    def aksCluster    = param.get("aksCluster")
    def imageName     = param.get("imageName", "name")
    def imageTag      = param.get("imageTag", "latest")
    def AZURE_SUBSCRIPTION_ID = param.get("AZURE_SUBSCRIPTION_ID")
    def AZURE_TENANT_ID = param.get("AZURE_TENANT_ID")

    docker.image('microsoft/azure-cli:latest').inside('-u root:root') {

        withCredentials([usernamePassword(credentialsId: azCredentials,
                                          usernameVariable: 'azUser',
                                          passwordVariable: 'azPass')]) {

            sh "az login --service-principal -u ${azUser} -p ${azPass} -t ${AZURE_TENANT_ID}"
            sh "az account set -s ${AZURE_SUBSCRIPTION_ID}"
            sh "az aks install-cli"
            sh "az aks get-credentials --resource-group ${resourceGroup} --name ${aksCluster}"
            sh "kubectl get nodes"

        }

    }
}