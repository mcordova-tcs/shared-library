def build(Map param = [:]) {
    def _version    = param.get("version", "jdk8")
    def _source     = param.get("source", "./")
    docker.image("gradle:${_version}").inside {
        dir(_source) {
            sh "gradle clean build -x test"
        }
    }
}

def test(Map param = [:]) {
    def _version    = param.get("version", "jdk8")
    def _source     = param.get("source", "./")
    docker.image("gradle:${_version}").inside {
        dir(_source) {
            sh "gradle test"
        }
    }
}