def build(Map param = [:]) {
    def _version    = param.get("version", "3.6.3-openjdk-8")
    def _source     = param.get("source", "./")
    
    docker.image("maven:${_version}").inside {
        dir(_source) {
            sh "mvn package"
        }
    }
}