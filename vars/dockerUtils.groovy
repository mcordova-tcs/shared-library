def buildAndPush(Map param = [:]) {
    def registryServer      = param.get("registryServer")
    def imageName           = param.get("imageName")
    def imageTag            = param.get("imageTag")
    def registryCredentials = param.get("registryCredentials")

    docker.image('docker:dind').inside('-u root:root -v /var/run/docker.sock:/var/run/docker.sock') {
        withCredentials([usernamePassword(credentialsId: registryCredentials,
                                          usernameVariable: 'registryUser',
                                          passwordVariable: 'registryPass')]) {
            sh "docker login -u ${registryUser} -p ${registryPass} ${registryServer}"
            sh "docker build -t ${registryServer}/${imageName}:${imageTag} ."
            sh "docker push ${registryServer}/${imageName}:${imageTag}"
        }
    }
}